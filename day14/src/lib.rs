#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Reindeer<'a> {
    name: &'a str,
    speed: u8,
    time: u8,
    rest_time: u8,
}

impl<'a> Reindeer<'a> {
    pub fn dist_in(&self, time: u16) -> u16 {
        let speed = self.speed as u16;

        let cycle_len = (self.time + self.rest_time) as u16;
        let cycle_gain = self.time as u16 * speed;

        let cycle_count = time / cycle_len;
        let leftover = (time % cycle_len).min(self.time as u16);

        (cycle_gain * cycle_count) + (leftover * speed)
    }

    pub fn get_name(&self) -> &'a str {
        self.name
    }
}

pub mod parser {
    use crate::Reindeer;
    pub use nom::error::Error as NomErr;
    use nom::{
        bytes::complete::tag,
        character::complete::{alpha1, line_ending, u8 as nomu8},
        combinator::{all_consuming, map},
        multi::{many0, separated_list0},
        sequence::{delimited, terminated, tuple},
        Finish, IResult,
    };

    fn rest_time(input: &str) -> IResult<&str, u8> {
        delimited(tag(" but then must rest for "), nomu8, tag(" seconds."))(input)
    }

    fn name(input: &str) -> IResult<&str, &str> {
        alpha1(input)
    }

    fn speed(input: &str) -> IResult<&str, u8> {
        delimited(tag(" can fly "), nomu8, tag(" km/s"))(input)
    }

    fn time(input: &str) -> IResult<&str, u8> {
        delimited(tag(" for "), nomu8, tag(" seconds,"))(input)
    }

    fn reindeer(input: &str) -> IResult<&str, Reindeer> {
        map(
            tuple((name, speed, time, rest_time)),
            |(name, speed, time, rest_time)| Reindeer {
                name,
                speed,
                time,
                rest_time,
            },
        )(input)
    }

    fn list(input: &str) -> IResult<&str, Vec<Reindeer>> {
        separated_list0(line_ending, reindeer)(input)
    }

    pub fn data(input: &str) -> Result<Vec<Reindeer>, NomErr<&str>> {
        all_consuming(terminated(list, many0(line_ending)))(input)
            .finish()
            .map(|(_, v)| v)
    }
}
