use day14::*;
use itertools::Itertools;
use std::fs;

const TIME: u16 = 2503;

fn main() {
    let input = fs::read_to_string("inputs/day14.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!(
        "Part 1: {}",
        input.iter().map(|r| r.dist_in(TIME)).max().unwrap()
    );
    println!(
        "Part 2: {:?}",
        (1..=TIME)
            .flat_map(|i| input.iter().max_set_by_key(|r| r.dist_in(i)))
            .counts()
            .into_iter()
            .max_by_key(|(_r, s)| *s)
            .unwrap()
            .1
    );
}
