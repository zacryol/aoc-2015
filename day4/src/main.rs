use rayon::prelude::*;
use std::fs;

fn solve(num_zeros: usize, input: &str) -> Option<usize> {
    let input = input.trim();
    let pat = vec!['0'; num_zeros].into_iter().collect::<String>();
    (0..=usize::MAX)
        .into_par_iter()
        .find_first(|i| format!("{:x}", md5::compute(format!("{input}{i}"))).starts_with(&pat))
}

fn main() {
    let input = fs::read_to_string("inputs/day4.txt").unwrap();
    (1..=2)
        .into_par_iter()
        .map(|i| (i, solve(i + 4, &input).unwrap()))
        .collect::<Vec<_>>()
        .into_iter()
        .for_each(|(i, res)| println!("Part {i}: {res}"));
}
