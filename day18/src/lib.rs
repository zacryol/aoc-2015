use rayon::prelude::*;

type GridRow = Box<[bool]>;
type Grid = Box<[GridRow]>;
type Point = (usize, usize);

pub fn grid_iter<F>(first: Grid, f: F) -> impl Iterator<Item = Grid>
where
    F: Fn(Grid) -> Grid,
{
    std::iter::successors(Some(f(first)), move |g| Some(f(successor(g))))
}

pub fn count_lights(g: &Grid) -> usize {
    g.par_iter()
        .flat_map(|r| r.par_iter())
        .filter(|on| **on)
        .count()
}

pub fn corners_on(mut g: Grid) -> Grid {
    let x = g[0].len() - 1;
    let y = g.len() - 1;

    g[y][x] = true;
    g[0][x] = true;
    g[y][0] = true;
    g[0][0] = true;

    g
}

fn successor(g: &Grid) -> Grid {
    g.par_iter()
        .enumerate()
        .map(|(y, r)| {
            r.par_iter()
                .enumerate()
                .map(|(x, _)| next_value(g, (x, y)))
                .collect::<Vec<_>>()
                .into_boxed_slice()
        })
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

fn next_value(g: &Grid, (x, y): Point) -> bool {
    let expand_one = |a: usize| (-1..=1isize).filter_map(move |i| a.checked_add_signed(i));
    let neighbor_count = expand_one(x)
        .flat_map(|xx| expand_one(y).zip(std::iter::repeat(xx)))
        .map(|(yy, xx)| (xx, yy))
        .filter(|p| *p != (x, y))
        .filter_map(|(xx, yy)| g.get(yy).and_then(|r| r.get(xx)))
        .filter(|p| **p)
        .count();
    matches!((neighbor_count, g[y][x]), (2..=3, true) | (3, false))
}

pub mod parser {
    use crate::{Grid, GridRow};
    use nom::{
        character::complete::{line_ending, none_of},
        combinator::{all_consuming, map},
        error::Error as NomErr,
        multi::{many1, separated_list1},
        Finish, IResult,
    };

    fn light(input: &str) -> IResult<&str, bool> {
        map(none_of("\n"), |c| c == '#')(input)
    }

    fn row(input: &str) -> IResult<&str, GridRow> {
        many1(light)(input).map(|(s, v)| (s, v.into_boxed_slice()))
    }

    fn grid(input: &str) -> IResult<&str, Vec<GridRow>> {
        separated_list1(line_ending, row)(input)
    }

    pub fn read_data(input: &str) -> Result<Grid, NomErr<&str>> {
        all_consuming(grid)(input.trim())
            .finish()
            .map(|(_, g)| g.into_boxed_slice())
    }
}
