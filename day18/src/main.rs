use day18::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day18.txt").unwrap();
    let input = parser::read_data(&input).unwrap();
    println!(
        "Part 1: {}",
        count_lights(&grid_iter(input.clone(), |g| g).take(101).last().unwrap())
    );
    println!(
        "Part 2: {}",
        count_lights(&grid_iter(input, corners_on).take(101).last().unwrap())
    );
}
