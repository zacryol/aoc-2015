use std::collections::HashMap;
type Data<'a> = HashMap<&'a str, u16>;

pub fn matches_data(sue: &Data) -> bool {
    sue.iter().all(|(k, v)| match *k {
        "children" => *v == 3,
        "cats" => *v == 7,
        "samoyeds" => *v == 2,
        "pomeranians" => *v == 3,
        "akitas" => *v == 0,
        "vizslas" => *v == 0,
        "goldfish" => *v == 5,
        "trees" => *v == 3,
        "cars" => *v == 2,
        "perfumes" => *v == 1,
        _ => true,
    })
}

pub fn matches_data_2(sue: &Data) -> bool {
    sue.iter().all(|(k, v)| match *k {
        "children" => *v == 3,
        "cats" => *v > 7,
        "samoyeds" => *v == 2,
        "pomeranians" => *v < 3,
        "akitas" => *v == 0,
        "vizslas" => *v == 0,
        "goldfish" => *v < 5,
        "trees" => *v > 3,
        "cars" => *v == 2,
        "perfumes" => *v == 1,
        _ => true,
    })
}

pub mod parser {
    use std::collections::HashMap;

    pub use nom::error::Error as NomErr;
    use nom::{
        bytes::complete::tag,
        character::complete::{alpha1, line_ending, u16 as nomu16},
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::{delimited, separated_pair, tuple},
        Finish, IResult,
    };

    use crate::Data;

    fn id(input: &str) -> IResult<&str, u16> {
        delimited(tag("Sue "), nomu16, tag(": "))(input)
    }

    fn key_val(input: &str) -> IResult<&str, (&str, u16)> {
        separated_pair(alpha1, tag(": "), nomu16)(input)
    }

    fn values(input: &str) -> IResult<&str, HashMap<&str, u16>> {
        map(separated_list1(tag(", "), key_val), |v| {
            v.into_iter().collect()
        })(input)
    }

    fn sue(input: &str) -> IResult<&str, (u16, HashMap<&str, u16>)> {
        tuple((id, values))(input)
    }

    pub fn data(input: &str) -> Result<Vec<(u16, Data)>, NomErr<&str>> {
        all_consuming(separated_list1(line_ending, sue))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}

