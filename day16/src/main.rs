use day16::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day16.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!(
        "Part 1: {}",
        input.iter().find(|(_, s)| matches_data(s)).unwrap().0
    );
    println!(
        "Part 1: {}",
        input.iter().find(|(_, s)| matches_data_2(s)).unwrap().0
    );
}
