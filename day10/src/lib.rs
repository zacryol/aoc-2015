use itertools::Itertools;

/*
 * didn't end up needing this
fn take_one(s: &[u8]) -> Option<((u8, u8), &[u8])> {
    let [i, rest @ ..] = s else {
        return None
    };
    Some(match rest {
        [a, b, tail @ ..] if a == i && b == i => ((b'3', *i), tail),
        [a, tail @ ..] if a == i => ((b'2', *i), tail),
        [tail @ ..] => ((b'1', *i), tail),
    })
}
*/

/*
 * This one was over 10x slower
pub fn look_and_say(n: &[u8]) -> Vec<u8> {
    n.iter()
        .map(|b| vec![*b])
        .coalesce(|v1, v2| {
            if v1.iter().chain(v2.iter()).all_equal() {
                Ok(v1.into_iter().chain(v2.into_iter()).collect())
            } else {
                Err((v1, v2))
            }
        })
        .map(|v| take_one(&v).unwrap().0)
        .flat_map(|(a, b)| [a, b])
        .collect()
}*/

pub fn look_and_say_alt(n: &[u8]) -> Vec<u8> {
    n.iter()
        .map(|b| (1, b))
        .coalesce(|t1, t2| {
            if t1.1 == t2.1 {
                Ok((t1.0 + t2.0, t1.1))
            } else {
                Err((t1, t2))
            }
        })
        .flat_map(|(a, b)| [a + b'0', *b])
        .collect()
}
