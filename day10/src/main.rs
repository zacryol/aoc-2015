use day10::*;

fn main() {
    let input = std::fs::read_to_string("inputs/day10.txt").unwrap();
    let input = input.trim();
    let mut num = input.as_bytes().to_owned();
    for _ in 0..40 {
        num = look_and_say_alt(&num);
    }
    println!("Part 1: {}", num.len());
    for _ in 0..10 {
        num = look_and_say_alt(&num);
    }
    println!("Part 2: {}", num.len());
}
