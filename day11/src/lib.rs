use itertools::Itertools;
static BANNED: &[u8] = &[b'i', b'o', b'l'];

fn has_increasing_straight(p: &[u8]) -> bool {
    p.iter()
        .tuple_windows()
        .any(|(a, b, c)| a + 1 == *b && b + 1 == *c)
}

fn no_banned_letters(p: &[u8]) -> bool {
    p.iter().all(|c| BANNED.iter().all(|b| b != c))
}

fn has_two_pair(p: &[u8]) -> bool {
    p.iter()
        .tuple_windows()
        .enumerate()
        .filter(|(_i, (a, b))| a == b)
        .map(|(i, _)| i)
        .minmax()
        .into_option()
        .map(|(a, b)| a.abs_diff(b) > 1)
        .unwrap_or(false)
}

fn is_valid(p: &[u8; 8]) -> bool {
    has_increasing_straight(p) && no_banned_letters(p) && has_two_pair(p)
}

pub fn get_next_password(current: [u8; 8]) -> [u8; 8] {
    PasswordIncr(current).find(is_valid).unwrap()
}

struct PasswordIncr([u8; 8]);

impl Iterator for PasswordIncr {
    type Item = [u8; 8];

    fn next(&mut self) -> Option<Self::Item> {
        self.0[7] += 1;
        for i in (0..7).rev() {
            if self.0[i + 1] > b'z' {
                self.0[i] += 1;
                self.0[i + 1] = b'a';
            }
        }
        // Hard skip on banned letters
        for i in 0..8 {
            if !no_banned_letters(&[self.0[i]]) {
                self.0[i] += 1;
                self.0[(i + 1)..].iter_mut().for_each(|b| *b = b'a');
            }
        }
        Some(self.0)
    }
}
