use day11::*;
use std::fs;

fn main() {
    let input: [u8; 8] = {
        let input = fs::read_to_string("inputs/day11.txt").unwrap();
        input.as_bytes()[0..8].try_into().unwrap()
    };
    let p1 = get_next_password(input);
    let p2 = get_next_password(p1);
    println!("Part 1: {}", std::str::from_utf8(&p1).unwrap());
    println!("Part 2: {}", std::str::from_utf8(&p2).unwrap());
}
