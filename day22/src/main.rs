use day22::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day22.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!("Part 1: {}", get_cheapest_win(input, 0));
    println!("Part 2: {}", get_cheapest_win(input, 1));
}
