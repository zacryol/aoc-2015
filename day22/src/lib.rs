pub fn get_cheapest_win(boss: (u8, u8), penalty: u8) -> u16 {
    let start = GameState::init(boss, penalty);
    fn cheapest_helper(gs: GameState) -> Option<u16> {
        if gs.boss_hp == 0 {
            return Some(gs.used_mana);
        }
        if gs.player_hp == 0 {
            return None;
        }
        gs.successors().filter_map(cheapest_helper).min()
    }
    cheapest_helper(start).unwrap()
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct GameState {
    player_mana: u16,
    used_mana: u16,
    player_hp: u8,
    boss_hp: u8,
    boss_attack: u8,
    shield_timer: u8,
    poison_timer: u8,
    recharge_timer: u8,
    penalty: u8,
}

impl GameState {
    fn init(boss: (u8, u8), penalty: u8) -> Self {
        Self {
            player_mana: 500,
            used_mana: 0,
            player_hp: 50,
            boss_hp: boss.0,
            boss_attack: boss.1,
            shield_timer: 0,
            poison_timer: 0,
            recharge_timer: 0,
            penalty,
        }
    }

    fn successors(self) -> impl Iterator<Item = Self> {
        Spell::iter().filter_map(move |s| self.try_spell(s))
    }

    fn try_spell(&self, spell: Spell) -> Option<Self> {
        Some(
            self.take_penalty()
                .tick_timers()
                .take_spell_cost(spell)?
                .add_effect(spell)
                .tick_timers()
                .boss_damage(),
        )
    }

    fn boss_damage(&self) -> Self {
        let mut s = *self;
        s.player_hp = s.player_hp.saturating_sub(
            self.boss_attack
                .saturating_sub(if self.shield_timer != 0 { 7 } else { 0 })
                .max(1),
        );
        s
    }

    fn add_effect(&self, spell: Spell) -> Self {
        let mut s = *self;
        match spell {
            Spell::Missile => {
                s.boss_hp = s.boss_hp.saturating_sub(4);
            }
            Spell::Drain => {
                s.boss_hp = s.boss_hp.saturating_sub(2);
                s.player_hp += 2;
            }
            Spell::Shield => {
                s.shield_timer = 6;
            }
            Spell::Poison => {
                s.poison_timer = 6;
            }
            Spell::Recharge => {
                s.recharge_timer = 5;
            }
        };
        s
    }

    fn tick_timers(&self) -> Self {
        Self {
            player_mana: self.player_mana + if self.recharge_timer != 0 { 101 } else { 0 },
            used_mana: self.used_mana,
            player_hp: self.player_hp,
            boss_hp: self
                .boss_hp
                .saturating_sub(if self.poison_timer != 0 { 3 } else { 0 }),
            boss_attack: self.boss_attack,
            shield_timer: self.shield_timer.saturating_sub(1),
            poison_timer: self.poison_timer.saturating_sub(1),
            recharge_timer: self.recharge_timer.saturating_sub(1),
            penalty: self.penalty,
        }
    }

    fn take_spell_cost(&self, spell: Spell) -> Option<Self> {
        let mut s = *self;
        s.player_mana = s.player_mana.checked_sub(spell.cost())?;
        s.used_mana += spell.cost();
        Some(s)
    }

    fn take_penalty(mut self) -> Self {
        self.player_hp = self.player_hp.saturating_sub(self.penalty);
        self
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, enum_iterator::Sequence)]
enum Spell {
    Missile,
    Drain,
    Shield,
    Poison,
    Recharge,
}

impl Spell {
    fn cost(&self) -> u16 {
        match self {
            Self::Missile => 53,
            Self::Drain => 73,
            Self::Shield => 113,
            Self::Poison => 173,
            Self::Recharge => 229,
        }
    }

    fn iter() -> impl Iterator<Item = Self> {
        enum_iterator::all::<Self>()
    }
}

pub mod parser {
    use nom::{
        bytes::complete::tag,
        character::complete::{multispace1, u8 as nomu8},
        combinator::all_consuming,
        error::Error as NomErr,
        sequence::{preceded, separated_pair},
        Finish,
    };

    pub fn data(input: &str) -> Result<(u8, u8), NomErr<&str>> {
        all_consuming(separated_pair(
            preceded(tag("Hit Points: "), nomu8),
            multispace1,
            preceded(tag("Damage: "), nomu8),
        ))(input.trim())
        .finish()
        .map(|(_i, t)| t)
    }
}
