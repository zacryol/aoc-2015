use day9::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day9.txt").unwrap();
    let graph = get_graph_data(&input);
    let paths = paths(&graph);
    let (s, l) = end_paths(&paths, &graph);
    println!("Part 1: {}", s.dist(&graph));
    println!("Part 2: {}", l.dist(&graph));
}
