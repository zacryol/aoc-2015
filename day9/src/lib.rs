use itertools::*;
use std::{
    collections::{HashMap, VecDeque},
    rc::Rc,
};

pub type Graph<'a> = HashMap<&'a str, HashMap<&'a str, u16>>;

pub fn get_graph_data(input: &str) -> Graph {
    input
        .lines()
        .map(|l| l.split_whitespace().collect::<Vec<_>>())
        .filter_map(|v| match v[..] {
            [from, "to", to, "=", dist] => Some((from, to, dist.parse().unwrap())),
            _ => None,
        })
        .flat_map(|(from, to, dst)| [(from, to, dst), (to, from, dst)])
        .map(|(from, to, dst)| (from, (to, dst)))
        .into_grouping_map()
        .collect::<HashMap<_, _>>()
}

#[derive(Debug)]
pub enum PathNode<'a> {
    Nil,
    Cons(Rc<Point<'a>>),
}

impl<'a> PathNode<'a> {
    fn has(&self, city: &str) -> bool {
        match self {
            Self::Nil => false,
            Self::Cons(p) if p.0 == city => true,
            Self::Cons(p) => p.1.has(city),
        }
    }

    pub fn dist(&self, g: &Graph) -> u16 {
        match self {
            Self::Nil => 0,
            Self::Cons(p) => match &p.1 {
                Self::Nil => 0,
                Self::Cons(p2) => g[p.0][p2.0] + p.1.dist(g),
            },
        }
    }
}

#[derive(Debug)]
pub struct Point<'a>(&'a str, PathNode<'a>);

fn get_successors<'a>(p: &Rc<Point<'a>>, map: &'a Graph) -> Vec<Rc<Point<'a>>> {
    map.keys()
        .filter(|c| **c != p.0 && !p.1.has(c))
        .map(|c| Rc::new(Point(c, PathNode::Cons(Rc::clone(p)))))
        .collect()
}

fn initial_points<'a>(data: &'a Graph) -> Vec<Rc<Point<'a>>> {
    data.keys()
        .map(|pn| Rc::new(Point(pn, PathNode::Nil)))
        .collect()
}

pub fn paths<'a>(g: &'a Graph) -> Vec<PathNode<'a>> {
    let mut q = VecDeque::from(initial_points(g));
    let mut output = Vec::new();
    while let Some(p) = q.pop_front() {
        if g.keys().all(|c| p.1.has(c) || p.0 == *c) {
            output.push(PathNode::Cons(p));
            continue;
        }
        q.extend(get_successors(&p, g));
    }
    output
}

pub fn end_paths<'a>(pns: &'a [PathNode<'a>], g: &Graph) -> (&'a PathNode<'a>, &'a PathNode<'a>) {
    pns.iter()
        .minmax_by_key(|pn| pn.dist(g))
        .into_option()
        .unwrap()
}
