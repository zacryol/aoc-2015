use std::collections::HashMap;

type WireId<'a> = &'a str;

#[derive(Debug)]
enum Op<'a> {
    Num(u16),
    Not(WireId<'a>),
    Noop(WireId<'a>),
    And { lhs: WireId<'a>, rhs: WireId<'a> },
    Or { lhs: WireId<'a>, rhs: WireId<'a> },
    LShift { lhs: WireId<'a>, rhs: u16 },
    RShift { lhs: WireId<'a>, rhs: u16 },
    OneAnd(WireId<'a>),
}

#[derive(Debug)]
pub struct Wire<'a> {
    name: WireId<'a>,
    input: Op<'a>,
}

pub fn eval<'a>(
    w: WireId,
    data: &HashMap<WireId<'a>, Wire<'a>>,
    cache: &mut HashMap<WireId<'a>, u16>,
) -> u16 {
    if let Some(v) = cache.get(w) {
        return *v;
    }
    let w = &data[w];
    let value = match &w.input {
        Op::Num(n) => *n,
        Op::Not(iw) => !eval(iw, data, cache),
        Op::Noop(iw) => eval(iw, data, cache),
        Op::And { lhs, rhs } => eval(lhs, data, cache) & eval(rhs, data, cache),
        Op::Or { lhs, rhs } => eval(lhs, data, cache) | eval(rhs, data, cache),
        Op::LShift { lhs, rhs } => eval(lhs, data, cache) << rhs,
        Op::RShift { lhs, rhs } => eval(lhs, data, cache) >> rhs,
        Op::OneAnd(iw) => eval(iw, data, cache) & 1,
    };
    cache.insert(w.name, value);
    value
}

pub fn rewire_pt2<'a>(
    mut data: HashMap<WireId<'a>, Wire<'a>>,
    new_val: u16,
) -> HashMap<WireId<'a>, Wire<'a>> {
    data.get_mut("b").unwrap().input = Op::Num(new_val);
    data
}

pub mod parser {
    use std::collections::HashMap;

    use crate::{Op, Wire, WireId};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, line_ending, u16 as nomu16},
        combinator::map,
        multi::{many0, separated_list0},
        sequence::{preceded, separated_pair},
        Finish, IResult,
    };

    fn wire_id(input: &str) -> IResult<&str, WireId> {
        alpha1(input).map(|(input, id)| (input, id))
    }

    fn op(input: &str) -> IResult<&str, Op> {
        let op_num = |s| map(nomu16, Op::Num)(s);
        let op_not = |s| map(preceded(tag("NOT "), wire_id), Op::Not)(s);
        let op_one_and = |s| map(preceded(tag("1 AND "), wire_id), Op::OneAnd)(s);
        let noop = |s| map(wire_id, Op::Noop)(s);

        let op_and = |s| {
            map(
                separated_pair(wire_id, tag(" AND "), wire_id),
                |(lhs, rhs)| Op::And { lhs, rhs },
            )(s)
        };
        let op_or = |s| {
            map(
                separated_pair(wire_id, tag(" OR "), wire_id),
                |(lhs, rhs)| Op::Or { lhs, rhs },
            )(s)
        };

        let op_lshift = |s| {
            map(
                separated_pair(wire_id, tag(" LSHIFT "), nomu16),
                |(lhs, rhs)| Op::LShift { lhs, rhs },
            )(s)
        };
        let op_rshift = |s| {
            map(
                separated_pair(wire_id, tag(" RSHIFT "), nomu16),
                |(lhs, rhs)| Op::RShift { lhs, rhs },
            )(s)
        };

        alt((
            op_one_and, op_not, op_num, op_and, op_or, op_lshift, op_rshift, noop,
        ))(input)
    }

    fn wire(input: &str) -> IResult<&str, Wire> {
        map(separated_pair(op, tag(" -> "), wire_id), |(input, name)| {
            Wire { name, input }
        })(input)
    }

    fn wires(input: &str) -> IResult<&str, Vec<Wire>> {
        separated_list0(many0(line_ending), wire)(input)
    }

    pub fn build_wires(input: &str) -> Option<HashMap<WireId, Wire>> {
        wires(input)
            .finish()
            .ok()
            .map(|(_i, v)| v.into_iter().map(|w| (w.name, w)).collect())
    }
}
