use day7::*;
use std::{collections::HashMap, fs};

fn main() {
    let input = fs::read_to_string("inputs/day7.txt").unwrap();
    let input = parser::build_wires(&input).unwrap();
    let a_val = eval("a", &input, &mut HashMap::default());
    println!("Part 1: {a_val}");
    let input = rewire_pt2(input, a_val);
    let a_val = eval("a", &input, &mut HashMap::default());
    println!("Part 2: {a_val}");
}
