use itertools::Itertools;
use std::fs;

type NumData = (u16, Vec<u16>);

pub fn get_data() -> NumData {
    let mut nums: Vec<_> = fs::read_to_string("inputs/day24.txt")
        .unwrap()
        .lines()
        .filter_map(|l| l.parse().ok())
        .collect();
    nums.sort();
    (nums.iter().sum(), nums)
}

fn find_len(real_sum: u16, nums: &[u16]) -> Option<usize> {
    Some(
        nums.iter()
            .powerset()
            .find(|g| g.iter().cloned().sum::<u16>() == real_sum)?
            .len(),
    )
}

pub fn min_qe_3_groups(sum: u16, nums: &[u16]) -> Option<u64> {
    let sum = sum / 3;
    let len = find_len(sum, nums)?;
    nums.iter()
        .combinations(len)
        .filter(|g| g.iter().cloned().sum::<u16>() == sum)
        .filter(|g| {
            // Make sure a second (and therefore a third) group
            // can be made from the remainder.
            nums.iter()
                .filter(|n| !g.contains(n))
                .powerset()
                .any(|set| set.iter().cloned().sum::<u16>() == sum)
        })
        .map(|g| g.iter().map(|x| **x as u64).product::<u64>())
        .min()
}

pub fn min_qe_4_groups(sum: u16, nums: &[u16]) -> Option<u64> {
    let sum = sum / 4;
    let len = find_len(sum, nums)?;
    nums.iter()
        .combinations(len)
        .filter(|g| g.iter().cloned().sum::<u16>() == sum)
        .filter(|g| {
            // Only checking if a second group can be made with the
            // same sum happened to work fine for me; no guarantees
            // it will for all inputs.
            nums.iter()
                .filter(|n| !g.contains(n))
                .powerset()
                .any(|set| set.iter().cloned().sum::<u16>() == sum)
        })
        .map(|g| g.iter().map(|x| **x as u64).product::<u64>())
        .min()
}
