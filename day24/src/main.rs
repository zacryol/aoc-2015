use day24::*;

fn main() {
    let (s, n) = get_data();
    println!("Part 1: {}", min_qe_3_groups(s, &n).unwrap());
    println!("Part 2: {}", min_qe_4_groups(s, &n).unwrap());
}
