use std::fs;
use std::ops::ControlFlow;

fn main() {
    let input = fs::read_to_string("inputs/day1.txt").unwrap();
    let directions = || {
        input.chars().filter_map(|c| match c {
            '(' => Some(1),
            ')' => Some(-1),
            _ => None,
        })
    };
    println!("Part 1: {}", directions().sum::<i32>());
    println!("Part 2: {}", {
        let ControlFlow::Break(idx) =
            directions()
                .zip(1..)
                .try_fold(0u32, |curr_floor, (step, idx)| {
                    curr_floor
                        .checked_add_signed(step)
                        .map_or_else(|| ControlFlow::Break(idx), ControlFlow::Continue)
                }) else { panic!() };
        idx
    });
}
