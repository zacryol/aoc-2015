use std::{collections::HashSet, iter::successors};

type Point = (i16, i16);

const NORTH: Point = (0, -1);
const SOUTH: Point = (0, 1);
const EAST: Point = (1, 0);
const WEST: Point = (-1, 0);

fn get_dir(c: char) -> Option<Point> {
    match c {
        'v' | 'V' => Some(SOUTH),
        '^' => Some(NORTH),
        '<' => Some(WEST),
        '>' => Some(EAST),
        _ => None,
    }
}

fn add_point(a: Point, b: Point) -> Point {
    (a.0 + b.0, a.1 + b.1)
}

fn get_house_positions(input: &str, n: usize) -> impl Iterator<Item = Point> + '_ {
    successors(Some(vec![Point::default(); n]), {
        let mut it = input.chars().filter_map(get_dir);
        move |sps| {
            Some(
                sps.iter()
                    .filter_map(|p| it.next().map(|n| add_point(n, *p)))
                    .collect::<Vec<_>>(),
            )
        }
    })
    .take_while(|v| !v.is_empty())
    .flatten()
}

pub fn get_house_count(input: &str, agents: usize) -> usize {
    get_house_positions(input, agents)
        .collect::<HashSet<_>>()
        .len()
}
