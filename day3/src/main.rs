use day3::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day3.txt").unwrap();
    (1..=2).for_each(|i| println!("Part {}: {}", i, get_house_count(&input, i)));
}
