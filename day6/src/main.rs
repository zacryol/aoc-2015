use day6::*;
use rayon::prelude::ParallelIterator;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day6.txt").unwrap();
    let input = input
        .lines()
        .filter_map(parser::get_inst)
        .collect::<Vec<_>>();
    macro_rules! run {
        ($pvalue:expr, $fn:expr) => {
            all_points().map(|p| input.iter().fold((p, $pvalue), blink_point($fn)).1)
        };
    }
    let (on_count, total_bright) = rayon::join(
        || run!(false, pt1_blink).filter(|on| *on).count(),
        || run!(0, pt2_blink).map(u64::from).sum::<u64>(),
    );
    println!("Part 1: {on_count}");
    println!("Part 2: {total_bright}");
}
