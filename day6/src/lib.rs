use rayon::prelude::*;

#[derive(Debug, Clone, Copy)]
pub enum InstructionType {
    On,
    Off,
    Toggle,
}

impl InstructionType {
    fn as_str(&self) -> &str {
        match self {
            InstructionType::On => "turn on",
            InstructionType::Off => "turn off",
            InstructionType::Toggle => "toggle",
        }
    }
}

type Point = (u16, u16);
type Rect = (Point, Point);
type Inst = (InstructionType, Rect);
type PointStatus<T> = (Point, T);

fn is_point_in_rect(p: Point, r: Rect) -> bool {
    let tl = r.0;
    let br = r.1;

    p.0 >= tl.0 && p.1 >= tl.1 && p.0 <= br.0 && p.1 <= br.1
}

pub fn all_points() -> impl ParallelIterator<Item = Point> {
    use std::iter::repeat;
    (0..1000)
        .into_par_iter()
        .flat_map_iter(|i| repeat(i).zip(0..1000))
}

pub fn blink_point<F, T>(f: F) -> impl Fn(PointStatus<T>, &Inst) -> PointStatus<T>
where
    F: Fn(InstructionType, T) -> T,
{
    move |(p, on), inst| {
        (
            p,
            if is_point_in_rect(p, inst.1) {
                f(inst.0, on)
            } else {
                on
            },
        )
    }
}

pub fn pt1_blink(it: InstructionType, on: bool) -> bool {
    match it {
        InstructionType::On => true,
        InstructionType::Off => false,
        InstructionType::Toggle => on ^ true,
    }
}

pub fn pt2_blink(it: InstructionType, val: u16) -> u16 {
    match it {
        InstructionType::On => val + 1,
        InstructionType::Off => val.saturating_sub(1),
        InstructionType::Toggle => val + 2,
    }
}

pub mod parser {
    use super::{Inst, InstructionType, Point, Rect};
    use nom::{
        branch::alt, bytes::complete::tag, character::complete::u16 as nomu16,
        sequence::separated_pair, Finish, IResult,
    };

    fn inst_type(input: &str) -> IResult<&str, InstructionType> {
        fn one_inst(which: InstructionType) -> impl Fn(&str) -> IResult<&str, InstructionType> {
            move |s| tag(which.as_str())(s).map(|(input, _)| (input, which))
        }

        alt((
            one_inst(InstructionType::Toggle),
            one_inst(InstructionType::Off),
            one_inst(InstructionType::On),
        ))(input)
    }

    fn point(input: &str) -> IResult<&str, Point> {
        separated_pair(nomu16, tag(","), nomu16)(input)
    }

    fn rect(input: &str) -> IResult<&str, Rect> {
        separated_pair(point, tag(" through "), point)(input)
    }

    fn inst(input: &str) -> IResult<&str, Inst> {
        separated_pair(inst_type, tag(" "), rect)(input)
    }

    pub fn get_inst(input: &str) -> Option<Inst> {
        inst(input).finish().ok().map(|(_s, i)| i)
    }
}
