use day21::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day21.txt").unwrap();
    let input = parser::boss_stats(&input).unwrap();
    println!(
        "Part 1: {}",
        Stats::cheapest_winner(&input).unwrap().get_cost()
    );
    println!(
        "Part 2: {}",
        Stats::priciest_loser(&input).unwrap().get_cost()
    );
}
