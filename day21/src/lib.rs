use std::ops::Add;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Stats {
    cost: u16,
    damage: u8,
    armor: u8,
    hp: u8,
}

impl Add for Stats {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let cost = self.cost + rhs.cost;
        let damage = self.damage + rhs.damage;
        let armor = self.armor + rhs.armor;
        let hp = self.hp + rhs.hp;
        Self {
            cost,
            damage,
            armor,
            hp,
        }
    }
}

impl Stats {
    fn dpt(&self, other: &Self) -> u8 {
        self.damage.saturating_sub(other.armor).max(1)
    }

    fn ttk(&self, other: &Self) -> u8 {
        other.hp / self.dpt(other)
    }

    /// Assuming `self` is the "player"
    fn is_win(&self, other: &Self) -> bool {
        self.ttk(other) <= other.ttk(self)
    }

    fn all_equips() -> impl Iterator<Item = Self> {
        use data::*;
        use std::iter::repeat;

        let all_weapons = || WEAPONS.iter();
        let all_armor = &|| ARMOR.iter();
        let all_rings = &|| {
            let all_rings = &|| RINGS.iter();
            all_rings()
                .flat_map(|r| repeat(r).zip(all_rings()))
                .filter(|(r1, r2)| r1 != r2)
                .chain(all_rings().map(|r| (r, &DEFAULT)))
                .chain([(&DEFAULT, &DEFAULT)].into_iter())
        };
        all_weapons()
            .flat_map(|w| repeat(w).zip(all_armor()))
            .flat_map(|wa| repeat(wa).zip(all_rings()))
            .map(|((w, a), (r1, r2))| (w, a, r1, r2))
            .map(|(w, a, r1, r2)| *w + *a + *r1 + *r2 + YOU_BASE)
    }

    pub fn cheapest_winner(boss: &Self) -> Option<Self> {
        Self::all_equips()
            .filter(|s| s.is_win(boss))
            .min_by_key(|s| s.cost)
    }

    pub fn priciest_loser(boss: &Self) -> Option<Self> {
        Self::all_equips()
            .filter(|s| !s.is_win(boss))
            .max_by_key(|s| s.cost)
    }

    pub fn get_cost(&self) -> u16 {
        self.cost
    }
}

mod data;

pub mod parser {

    use crate::Stats;
    use nom::{
        bytes::complete::tag,
        character::complete::{multispace0, u8 as nomu8},
        combinator::{all_consuming, map},
        error::{Error as NomErr, ParseError},
        sequence::{delimited, tuple},
        Finish, IResult, Parser,
    };

    fn stat<'a, E: ParseError<&'a str>>(s: &'a str) -> impl Parser<&'a str, u8, E> {
        delimited(tuple((tag(s), tag(": "))), nomu8, multispace0)
    }

    fn stats(input: &str) -> IResult<&str, Stats> {
        map(
            tuple((stat("Hit Points"), stat("Damage"), stat("Armor"))),
            |(h, d, a)| Stats {
                cost: 0,
                damage: d,
                armor: a,
                hp: h,
            },
        )(input)
    }

    pub fn boss_stats(input: &str) -> Result<Stats, NomErr<&str>> {
        all_consuming(stats)(input.trim())
            .finish()
            .map(|(_i, stats)| stats)
    }
}
