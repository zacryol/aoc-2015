use crate::Stats;

pub static DEFAULT: Stats = Stats {
    cost: 0,
    damage: 0,
    armor: 0,
    hp: 0,
};

pub static YOU_BASE: Stats = Stats {
    cost: 0,
    damage: 0,
    armor: 0,
    hp: 100,
};

pub static WEAPONS: &[Stats] = &[
    Stats {
        cost: 8,
        damage: 4,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 10,
        damage: 5,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 25,
        damage: 6,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 40,
        damage: 7,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 74,
        damage: 8,
        armor: 0,
        hp: 0,
    },
];

pub static ARMOR: &[Stats] = &[
    Stats {
        cost: 0,
        damage: 0,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 13,
        damage: 0,
        armor: 1,
        hp: 0,
    },
    Stats {
        cost: 31,
        damage: 0,
        armor: 2,
        hp: 0,
    },
    Stats {
        cost: 53,
        damage: 0,
        armor: 3,
        hp: 0,
    },
    Stats {
        cost: 75,
        damage: 0,
        armor: 4,
        hp: 0,
    },
    Stats {
        cost: 102,
        damage: 0,
        armor: 5,
        hp: 0,
    },
];

pub static RINGS: &[Stats] = &[
    Stats {
        cost: 25,
        damage: 1,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 50,
        damage: 2,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 100,
        damage: 3,
        armor: 0,
        hp: 0,
    },
    Stats {
        cost: 20,
        damage: 0,
        armor: 1,
        hp: 0,
    },
    Stats {
        cost: 40,
        damage: 0,
        armor: 2,
        hp: 0,
    },
    Stats {
        cost: 80,
        damage: 0,
        armor: 3,
        hp: 0,
    },
];
