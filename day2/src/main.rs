use day2::*;
use std::fs;
fn main() {
    let input = fs::read_to_string("inputs/day2.txt").unwrap();
    let presents_data = get_file_data(&input).unwrap();
    let f = || presents_data.iter();
    println!("Part 1: {}", f().map(get_needed_paper).sum::<Dimen>());
    println!("Part 2: {}", f().map(get_needed_ribbon).sum::<Dimen>());
}
