pub type Dimen = u32;
type Size = (Dimen, Dimen, Dimen);

pub use parser::get_file_data;

mod parser {
    use super::{Dimen, Size};
    use nom::{
        bytes::complete::tag,
        character::complete::{line_ending, u32 as nomu32},
        combinator::opt,
        multi::separated_list1,
        sequence::{terminated, tuple},
        Finish, IResult,
    };

    fn num(input: &str) -> IResult<&str, Dimen> {
        terminated(nomu32, opt(tag("x")))(input)
    }

    fn line(input: &str) -> IResult<&str, Size> {
        tuple((num, num, num))(input)
    }

    fn parse_file(input: &str) -> IResult<&str, Vec<Size>> {
        separated_list1(line_ending, line)(input)
    }

    pub fn get_file_data(input: &str) -> Option<Vec<Size>> {
        parse_file(input).finish().ok().map(|(_, v)| v)
    }
}

pub fn get_needed_paper(s: &Size) -> Dimen {
    let fac = get_faces_area(s);
    (get_sum(&fac) * 2) + get_smallest(&fac)
}

pub fn get_needed_ribbon(s: &Size) -> Dimen {
    get_smallest(&get_perimeters(s)) + get_volume(s)
}

fn get_faces_area(s: &Size) -> Size {
    (s.0 * s.1, s.1 * s.2, s.0 * s.2)
}

fn get_smallest(s: &Size) -> Dimen {
    s.0.min(s.1).min(s.2)
}

fn get_sum(s: &Size) -> Dimen {
    s.0 + s.1 + s.2
}

fn get_volume(s: &Size) -> Dimen {
    s.0 * s.1 * s.2
}

fn get_perimeters(s: &Size) -> Size {
    ((s.0 + s.1) * 2, (s.1 + s.2) * 2, (s.0 + s.2) * 2)
}
