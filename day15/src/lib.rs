use itertools::Itertools;
use std::ops::{Add, Mul};

#[derive(Debug)]
pub struct Ingredient<'a>(&'a str, IngStats);

impl<'a> Ingredient<'a> {
    pub fn vals(&self) -> IngStats {
        self.1
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Default)]
pub struct IngStats {
    capacity: i32,
    durability: i32,
    flavor: i32,
    texture: i32,
    calories: i32,
}

impl Add for IngStats {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let capacity = self.capacity + rhs.capacity;
        let durability = self.durability + rhs.durability;
        let flavor = self.flavor + rhs.flavor;
        let texture = self.texture + rhs.texture;
        let calories = self.calories + rhs.calories;

        Self {
            capacity,
            durability,
            flavor,
            texture,
            calories,
        }
    }
}

impl Mul<usize> for IngStats {
    type Output = Self;

    fn mul(self, rhs: usize) -> Self::Output {
        let rhs = rhs as i32;

        let capacity = self.capacity * rhs;
        let durability = self.durability * rhs;
        let flavor = self.flavor * rhs;
        let texture = self.texture * rhs;
        let calories = self.calories * rhs;

        Self {
            capacity,
            durability,
            flavor,
            texture,
            calories,
        }
    }
}

impl IngStats {
    fn final_score(self) -> i32 {
        self.capacity.max(0) * self.durability.max(0) * self.flavor.max(0) * self.texture.max(0)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IngCounts(Vec<usize>);

impl IngCounts {
    pub fn score(&self, ing: &[IngStats]) -> i32 {
        ing.iter()
            .zip(self.0.iter())
            .map(|(i, c)| *i * *c)
            .reduce(|i, i2| i + i2)
            .unwrap()
            .final_score()
    }

    pub fn total_calories(&self, ing: &[IngStats]) -> i32 {
        ing.iter()
            .zip(self.0.iter())
            .map(|(i, c)| *i * *c)
            .reduce(|i, i2| i + i2)
            .unwrap()
            .calories
    }

    pub fn successors(&self) -> Vec<Self> {
        (0..(self.0.len()))
            .permutations(2)
            .filter_map(|ud| {
                let [u, d] = ud[..] else {
                    return None
                };
                let mut v = self.clone();
                v.0[u] = v.0[u].checked_add(1)?;
                v.0[d] = v.0[d].checked_sub(1)?;
                Some(v)
            })
            .collect_vec()
    }

    pub fn successors_same_cal(&self, ing: &[IngStats]) -> Vec<Self> {
        let cal_val = self.total_calories(ing);
        let ing = {
            let mut ing = ing.to_owned();
            ing.sort_by_key(|i| i.calories);
            ing
        };
        // harcoding length 4
        let [a, b, c, d] = ing[..] else {
            return vec![];
        };
        let [oa, ob, oc, od] = self.0[..] else {
            return vec![]
        };

        // Based on patterns found in my input
        //
        // (b + d) % (cal(c) - cal(a)) [C(CA)]
        // (c + a) % (cal(d) - cal(b)) [C(DB)]
        // (c - b) % (cal(d) - cal(a)) [C(DA)]
        // (d - a) % (cal(c) - cal(b)) [C(CB)]
        // a + b + c + d
        // must all remain the same
        //
        // a = a; b += 1 => c -= (?), d += C(CB)
        // a += 1; b = b => c -= C(DA), d += (?)
        // c = c; d += 1 => a += (?), b -= C(DA)
        // d = d; c += 1 => a += C(CB), b -= (?)
        // in my input, C(CA) and C(DB) have the same value, so idk which need using

        let cca = (c.calories - a.calories) as usize;
        //let cdb = (d.calories - b.calories) as usize;
        let cda = (d.calories - a.calories) as usize;
        let ccb = (c.calories - b.calories) as usize;

        vec![
            Self(vec![oa, ob + 1, oc - cca, od + ccb]),
            Self(vec![oa, ob - 1, oc + cca, od - ccb]),
            Self(vec![oa + 1, ob, oc - cda, od + cca]),
            Self(vec![oa - 1, ob, oc + cda, od - cca]),
            Self(vec![oa + cca, ob - cda, oc, od + 1]),
            Self(vec![oa - cca, ob + cda, oc, od - 1]),
            Self(vec![oa + ccb, ob - cca, oc + 1, od]),
            Self(vec![oa - ccb, ob + cca, oc - 1, od]),
        ]
        .into_iter()
        // extra guard -- run in release mode
        .filter(|ic| ic.total_calories(&ing) == cal_val)
        .collect()
    }

    fn initial<F>(ing: &[IngStats], sum: usize, constriaint: F) -> Self
    where
        F: Fn(&Self, &[IngStats]) -> bool,
    {
        let len = ing.len();
        loop {
            let v = Self({
                let mut v = vec![sum; len];
                while v.iter().sum::<usize>() > sum {
                    let idx: usize = rand::random::<usize>() % len;
                    v[idx] = v[idx].saturating_sub(1);
                }
                v
            });
            if v.score(ing) > 0 && constriaint(&v, ing) {
                return v;
            }
        }
    }
}

pub fn climber<F, S, F2>(
    ing: &[IngStats],
    sum: usize,
    mut f: F,
    succ: S,
    initial_constraint: F2,
) -> IngCounts
where
    F: FnMut(&IngCounts, &[IngStats]) -> i32,
    S: Fn(&IngCounts) -> Vec<IngCounts>,
    F2: Fn(&IngCounts, &[IngStats]) -> bool,
{
    std::iter::successors(
        Some(IngCounts::initial(ing, sum, initial_constraint)),
        |ic| {
            succ(ic)
                .into_iter()
                .chain([ic.clone()])
                .max_by_key(|i| f(i, ing))
                .filter(|i| *i != *ic)
        },
    )
    .last()
    .unwrap()
}

pub mod parser {
    use crate::{IngStats, Ingredient};
    pub use nom::error::Error as NomErr;
    use nom::{
        bytes::complete::tag,
        character::complete::{alpha1, i32 as nomi32, line_ending},
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::tuple,
        sequence::{delimited, preceded, terminated},
        Finish, IResult,
    };

    fn name(input: &str) -> IResult<&str, &str> {
        terminated(alpha1, tag(":"))(input)
    }

    fn capacity(input: &str) -> IResult<&str, i32> {
        delimited(tag(" capacity "), nomi32, tag(","))(input)
    }

    fn durability(input: &str) -> IResult<&str, i32> {
        delimited(tag(" durability "), nomi32, tag(","))(input)
    }

    fn flavor(input: &str) -> IResult<&str, i32> {
        delimited(tag(" flavor "), nomi32, tag(","))(input)
    }

    fn texture(input: &str) -> IResult<&str, i32> {
        delimited(tag(" texture "), nomi32, tag(","))(input)
    }

    fn calories(input: &str) -> IResult<&str, i32> {
        preceded(tag(" calories "), nomi32)(input)
    }

    fn values(input: &str) -> IResult<&str, IngStats> {
        map(
            tuple((capacity, durability, flavor, texture, calories)),
            |(capacity, durability, flavor, texture, calories)| IngStats {
                capacity,
                durability,
                flavor,
                texture,
                calories,
            },
        )(input)
    }

    fn ingredient(input: &str) -> IResult<&str, Ingredient> {
        map(tuple((name, values)), |(n, v)| Ingredient(n, v))(input)
    }

    fn list(input: &str) -> IResult<&str, Vec<Ingredient>> {
        separated_list1(line_ending, ingredient)(input)
    }

    pub fn data(input: &str) -> Result<Vec<Ingredient>, NomErr<&str>> {
        all_consuming(list)(input.trim()).finish().map(|(_, v)| v)
    }
}
