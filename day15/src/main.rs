use day15::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day15.txt").unwrap();
    let input = parser::data(&input).unwrap();
    let ing = input.iter().map(Ingredient::vals).collect::<Vec<_>>();
    println!(
        "Part 1: {}",
        climber(
            &ing,
            100,
            IngCounts::score,
            IngCounts::successors,
            |_, _| true
        )
        .score(&ing)
    );
    // Not actually guaranteed due to random initial state; run it a few times.
    println!(
        "Part 2: {}",
        climber(
            &ing,
            100,
            IngCounts::score,
            |i| i.successors_same_cal(&ing),
            |i, ng| i.total_calories(ng) == 500
        )
        .score(&ing)
    );
}
