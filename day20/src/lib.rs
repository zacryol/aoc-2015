use integer_sqrt::IntegerSquareRoot;

pub fn presents(n: usize) -> usize {
    factors(n).sum::<usize>() * 10
}

pub fn presents_2(n: usize) -> usize {
    factors(n).filter(|x| *x >= n / 50).sum::<usize>() * 11
}

fn factors(n: usize) -> impl Iterator<Item = usize> {
    (1..=(n.integer_sqrt()))
        .filter(move |i| n % i == 0)
        .flat_map(move |i| [i, n / i])
}
