use day20::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day20.txt")
        .unwrap()
        .trim()
        .parse::<usize>()
        .unwrap();
    println!(
        "Part 1: {}",
        (1..)
            .map(|i| (i, presents(i)))
            .find(|(_i, p)| *p >= input)
            .unwrap()
            .0
    );
    println!(
        "Part 2: {}",
        (1..)
            .map(|i| (i, presents_2(i)))
            .find(|(_i, p)| *p >= input)
            .unwrap()
            .0
    );
}
