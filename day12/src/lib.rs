#[derive(Debug, PartialEq, Eq)]
pub enum Token<'a> {
    Num(i16),
    Str(&'a str),
    Arr(Vec<Token<'a>>),
    Object(Vec<(Token<'a>, Token<'a>)>),
}

impl<'a> Token<'a> {
    pub fn sum(&self) -> isize {
        match self {
            Self::Num(n) => *n as isize,
            Self::Str(_) => 0,
            Self::Arr(l) => l.iter().map(Self::sum).sum(),
            Self::Object(o) => o.iter().map(|(k, v)| k.sum() + v.sum()).sum(),
        }
    }

    pub fn sum_ignore_red(&self) -> isize {
        match self {
            Self::Num(n) => *n as isize,
            Self::Str(_) => 0,
            Self::Arr(l) => l.iter().map(Self::sum_ignore_red).sum(),
            Self::Object(_) if self.contains_red() => 0,
            Self::Object(o) => o
                .iter()
                .map(|(k, v)| k.sum_ignore_red() + v.sum_ignore_red())
                .sum(),
        }
    }

    fn contains_red(&self) -> bool {
        match self {
            Self::Object(o) => o.iter().map(|(_k, v)| v).any(|v| v == &Token::Str("red")),
            _ => false,
        }
    }
}

pub mod parser {
    use crate::Token;
    use nom::{
        branch::alt,
        bytes::complete::{tag, take_until},
        character::complete::i16 as nomi16,
        combinator::map,
        multi::separated_list0,
        sequence::{delimited, separated_pair},
        Finish, IResult,
    };

    fn num(input: &str) -> IResult<&str, Token> {
        map(nomi16, Token::Num)(input)
    }

    fn string(input: &str) -> IResult<&str, Token> {
        map(
            delimited(tag("\""), take_until("\""), tag("\"")),
            Token::Str,
        )(input)
    }

    fn array(input: &str) -> IResult<&str, Token> {
        map(
            delimited(tag("["), separated_list0(tag(","), token), tag("]")),
            Token::Arr,
        )(input)
    }

    fn obj(input: &str) -> IResult<&str, Token> {
        map(
            delimited(
                tag("{"),
                separated_list0(tag(","), separated_pair(token, tag(":"), token)),
                tag("}"),
            ),
            Token::Object,
        )(input)
    }

    fn token(input: &str) -> IResult<&str, Token> {
        alt((num, string, array, obj))(input)
    }

    pub fn data(input: &str) -> Result<(&str, Token), nom::error::Error<&str>> {
        token(input).finish()
    }
}
