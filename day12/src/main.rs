use day12::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day12.txt").unwrap();
    let (_, input) = parser::data(&input).unwrap();
    println!("Part 1: {}", input.sum());
    println!("Part 2: {}", input.sum_ignore_red());
}
