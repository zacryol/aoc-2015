use day17::*;
use itertools::Itertools;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day17.txt").unwrap();
    let input = liter_counts(&input);
    let combs = sums_to(150, &input)
        .into_iter()
        .map(|td| un_temp(&td))
        .collect::<Vec<_>>();
    println!("Part 1: {}", combs.len());
    println!(
        "Part 2: {}",
        combs.iter().map(containers_used).min_set().len()
    )
}
