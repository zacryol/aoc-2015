use itertools::Itertools;
use std::{
    cmp::Ordering,
    collections::{HashMap, VecDeque},
    iter::repeat_with,
};

type Data = HashMap<usize, usize>;
type TempData = HashMap<usize, Option<usize>>;

pub fn liter_counts(input: &str) -> Data {
    input
        .lines()
        .filter_map(|l| l.parse::<usize>().ok())
        .counts()
}

fn initial_state(data: &Data) -> TempData {
    data.keys().map(|k| (*k, None)).collect()
}

pub fn sums_to(sum: usize, data: &Data) -> Vec<TempData> {
    let mut q = VecDeque::from(vec![initial_state(data)]);
    let mut out = Vec::new();
    while let Some(c) = q.pop_back() {
        match capacity(&c).cmp(&sum) {
            Ordering::Less => q.extend(successors(&c, data)),
            Ordering::Equal => out.push(c),
            Ordering::Greater => continue,
        }
    }
    out
}

fn successors(curr: &TempData, totals: &Data) -> Vec<TempData> {
    curr.iter()
        .filter_map(|(k, v)| Some(*k).xor(*v))
        .max()
        .map(|k| {
            repeat_with(|| curr.clone())
                .zip(0..=(totals[&k]))
                .flat_map(|(mut d, v)| {
                    *d.get_mut(&k).unwrap() = Some(v);
                    repeat_with(move || d.clone()).take({
                        let n = totals[&k];
                        let k = v;
                        (1..=n).product::<usize>()
                            / ({ (1..=k).product::<usize>() * (1..=(n - k)).product::<usize>() })
                    })
                })
                .collect()
        })
        .unwrap_or_default()
}

fn capacity(data: &TempData) -> usize {
    data.iter().map(|(k, v)| k * v.unwrap_or(0)).sum()
}

pub fn un_temp(t: &TempData) -> Data {
    t.iter().map(|(k, v)| (*k, v.unwrap_or(0))).collect()
}

pub fn containers_used(d: &Data) -> usize {
    d.values().sum()
}
