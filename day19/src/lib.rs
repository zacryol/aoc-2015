use std::{cell::Cell, collections::HashSet};

type Rule<'a> = (&'a str, &'a str);

pub fn build_molecule(start: &str, molecule: &str, rules: &[Rule]) -> Option<usize> {
    let rules = {
        let mut rules = rules.to_owned();
        rules.sort_by_key(|r| r.1.len() - r.0.len());
        rules.reverse();
        rules
    };

    // Hacky recursive closure
    type Status<'a> = (&'a str, usize);
    type BuildFn<'a> = &'a dyn Fn(Status) -> Option<usize>;
    let build_cell: Cell<BuildFn> = Cell::new(&|_| None);
    let build_helper: BuildFn = &|(c, n)| {
        if c == start {
            return Some(n);
        }
        if c.len() < 2 {
            return None;
        }
        rules
            .iter()
            .flat_map(|r| reverse_replacements(r, c))
            .find_map(|s| build_cell.get()((&s, n + 1)))
    };

    build_cell.set(&build_helper);
    build_cell.get()((molecule, 0))
}

pub fn count_unique_replacements(rules: &[Rule], molecule: &str) -> usize {
    rules
        .iter()
        .flat_map(|r| replacements(*r, molecule))
        .collect::<HashSet<_>>()
        .len()
}

fn reverse_replacements<'a>(r: &'a Rule, molecule: &'a str) -> impl Iterator<Item = String> + 'a {
    let r = (r.1, r.0);
    replacements(r, molecule)
}

fn replacements<'a>(r: Rule<'a>, molecule: &'a str) -> impl Iterator<Item = String> + 'a {
    use nom::{
        bytes::complete::{tag, take_until},
        combinator::opt,
        multi::many0,
        sequence::tuple,
    };

    let fragments: nom::IResult<&'a str, Vec<(&'a str, Option<&'a str>)>> =
        many0(tuple((take_until(r.0), opt(tag(r.0)))))(molecule);
    let fragments = fragments
        .map(|(i, v)| {
            v.into_iter()
                .flat_map(|(a, b)| [a, b.unwrap_or("")])
                .chain([i])
        })
        .unwrap()
        .collect::<Vec<_>>();
    let replace_indices = fragments
        .clone()
        .into_iter()
        .enumerate()
        .filter(move |(_i, s)| *s == r.0)
        .map(|(i, _s)| i);

    replace_indices.map(move |i| {
        let mut frag = fragments.clone();
        frag[i] = r.1;
        frag.into_iter().collect::<String>()
    })
}

pub mod parser {
    use crate::Rule;
    use nom::{
        bytes::complete::tag,
        character::complete::{alpha1, line_ending, multispace0},
        combinator::all_consuming,
        error::Error as NomErr,
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
        Finish, IResult,
    };

    fn rule(input: &str) -> IResult<&str, Rule> {
        separated_pair(alpha1, tag(" => "), alpha1)(input)
    }

    fn rules(input: &str) -> IResult<&str, Vec<Rule>> {
        separated_list1(line_ending, rule)(input)
    }

    fn molecule(input: &str) -> IResult<&str, &str> {
        preceded(multispace0, alpha1)(input)
    }

    pub fn data(input: &str) -> Result<(Vec<Rule>, &str), NomErr<&str>> {
        all_consuming(tuple((rules, molecule)))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
