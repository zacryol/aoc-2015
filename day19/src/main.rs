use day19::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day19.txt").unwrap();
    let (rules, molecule) = parser::data(&input).unwrap();
    println!("Part 1: {}", count_unique_replacements(&rules, molecule));
    println!("Part 2: {}", build_molecule("e", molecule, &rules).unwrap());
}
