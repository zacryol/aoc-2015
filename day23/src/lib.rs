use std::{
    ops::{Index, IndexMut},
    rc::Rc,
};

#[derive(Debug, Clone)]
pub struct Cpu {
    registers: (usize, usize),
    pc: usize,
    inst: Rc<[Instr]>,
}

impl Cpu {
    pub fn init(code: &Rc<[Instr]>, a: usize) -> Result<Self, nom::error::Error<&str>> {
        Ok(Self {
            registers: (a, 0),
            pc: 0,
            inst: Rc::clone(code),
        })
    }

    pub fn exec(&self) -> usize {
        std::iter::successors(Some(self.clone()), Self::apply_instr)
            .last()
            .unwrap()[Reg::B]
    }

    fn apply_instr(&self) -> Option<Self> {
        let instr = self.inst.get(self.pc)?;
        let mut new = self.clone();
        new.pc = match instr {
            Instr::Hlf(r) => {
                new[*r] /= 2;
                self.pc + 1
            }
            Instr::Tpl(r) => {
                new[*r] *= 3;
                self.pc + 1
            }
            Instr::Inc(r) => {
                new[*r] += 1;
                self.pc + 1
            }
            Instr::Jmp(o) => self.pc.checked_add_signed(*o)?,
            Instr::Jie(r, o) => self.pc.checked_add_signed(match self[*r] % 2 {
                0 => *o,
                _ => 1,
            })?,
            Instr::Jio(r, o) => self.pc.checked_add_signed(match self[*r] {
                1 => *o,
                _ => 1,
            })?,
        };

        Some(new)
    }
}

impl Index<Reg> for Cpu {
    type Output = usize;

    fn index(&self, index: Reg) -> &Self::Output {
        match index {
            Reg::A => &self.registers.0,
            Reg::B => &self.registers.1,
        }
    }
}

impl IndexMut<Reg> for Cpu {
    fn index_mut(&mut self, index: Reg) -> &mut Self::Output {
        match index {
            Reg::A => &mut self.registers.0,
            Reg::B => &mut self.registers.1,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Reg {
    A,
    B,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Instr {
    Hlf(Reg),
    Tpl(Reg),
    Inc(Reg),
    Jmp(isize),
    Jie(Reg, isize),
    Jio(Reg, isize),
}

pub mod parser {
    use std::convert::TryInto;

    use crate::{Instr, Reg};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{i32 as nomi32, line_ending, multispace1},
        combinator::{all_consuming, map, map_res, value},
        error::{Error as NomErr, ParseError},
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
        Finish, IResult, Parser,
    };

    fn reg(input: &str) -> IResult<&str, Reg> {
        alt((value(Reg::A, tag("a")), value(Reg::B, tag("b"))))(input)
    }

    fn offset(input: &str) -> IResult<&str, isize> {
        map_res(nomi32, TryInto::try_into)(input)
    }

    fn instr(input: &str) -> IResult<&str, Instr> {
        fn inst_inner<'a, P, E, T, F>(name: &'a str, params: P, f: F) -> impl Parser<&str, Instr, E>
        where
            P: Parser<&'a str, T, E>,
            E: ParseError<&'a str>,
            F: Fn(T) -> Instr,
        {
            map(preceded(tuple((tag(name), multispace1)), params), f)
        }
        let ro = || separated_pair(reg, tuple((tag(","), multispace1)), offset);
        let hlf = inst_inner("hlf", reg, Instr::Hlf);
        let tpl = inst_inner("tpl", reg, Instr::Tpl);
        let inc = inst_inner("inc", reg, Instr::Inc);
        let jmp = inst_inner("jmp", offset, Instr::Jmp);
        let jie = inst_inner("jie", ro(), |(r, o)| Instr::Jie(r, o));
        let jio = inst_inner("jio", ro(), |(r, o)| Instr::Jio(r, o));

        alt((hlf, tpl, inc, jmp, jie, jio))(input)
    }

    pub fn code(input: &str) -> Result<Vec<Instr>, NomErr<&str>> {
        all_consuming(separated_list1(line_ending, instr))(input.trim())
            .finish()
            .map(|(_i, v)| v)
    }
}
