use day23::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day23.txt").unwrap();
    let input = parser::code(&input).unwrap().into_boxed_slice().into();
    let cpu = Cpu::init(&input, 0).unwrap();
    println!("Part 1: {}", cpu.exec());
    let cpu = Cpu::init(&input, 1).unwrap();
    println!("Part 2: {}", cpu.exec());
}
