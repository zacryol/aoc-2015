use day8::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day8.txt").unwrap();

    let (total_code, total_mem, total_esc) = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| (code_len(l), mem_len(l), escaped_len(l)))
        .reduce(|(c1, m1, e1), (c2, m2, e2)| (c1 + c2, m1 + m2, e1 + e2))
        .unwrap();
    let (total_code, total_mem, total_esc) =
        (total_code as isize, total_mem as isize, total_esc as isize);
    println!("Part 1: {}", total_code - total_mem);
    println!("Part 2: {}", total_esc - total_code);
}
