pub fn code_len(s: &str) -> usize {
    s.len()
}

pub fn mem_len(s: &str) -> usize {
    let s = s.as_bytes();
    std::iter::successors(Some(s), |ss| match ss {
        [b'\\', b'x', _, _, tail @ ..] => Some(tail),
        [b'\\', b'\\' | b'"', tail @ ..] => Some(tail),
        [_] => None,
        [_, tail @ ..] => Some(tail),
        [] => None,
    })
    .count()
        - 2
}

pub fn escaped_len(s: &str) -> usize {
    s.as_bytes().escape_ascii().count() + 2
}
