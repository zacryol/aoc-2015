use itertools::Itertools;
use std::collections::HashMap;

type EdgeMap<'a> = HashMap<(&'a str, &'a str), i32>;

fn nodes<'a>(e: &'a EdgeMap) -> impl Iterator<Item = &'a str> {
    e.keys()
        .map(|(a, _)| *a)
        .unique()
        .chain(e.keys().map(|(_, b)| *b).max())
}

pub fn build_edge_map(input: &str) -> Result<EdgeMap, parser::NomErr<&str>> {
    let list = parser::list(input)?;
    Ok(list
        .into_iter()
        .map(|(a, c, b)| ((a.min(b), a.max(b)), c))
        .into_grouping_map()
        .sum())
}

pub fn with_you<'a>(e: &'a EdgeMap) -> EdgeMap<'a> {
    let mut new_e = e.clone();
    nodes(e).map(|n| (n, "ZZZZ")).for_each(|t| {
        new_e.insert(t, 0);
    });
    new_e
}

fn gain(p: &[&str], e: &EdgeMap) -> isize {
    p.iter()
        .circular_tuple_windows()
        .map(|(a, b)| e[&(*a.min(b), *a.max(b))] as isize)
        .sum()
}

pub fn best_path_gain(e: &EdgeMap) -> isize {
    nodes(e)
        .permutations(nodes(e).count())
        .map(|p| gain(&p, e))
        .max()
        .unwrap()
}

mod parser {
    pub use nom::error::Error as NomErr;
    use nom::{
        bytes::complete::tag,
        character::complete::{alpha1, i32 as nomi32, line_ending},
        combinator::{all_consuming, map},
        multi::{many0, separated_list0},
        sequence::{delimited, separated_pair, terminated, tuple},
        Finish, IResult,
    };

    fn value(input: &str) -> IResult<&str, i32> {
        map(separated_pair(sign, tag(" "), nomi32), |(s, i)| s * i)(input)
    }

    fn sign(input: &str) -> IResult<&str, i32> {
        map(alpha1, |s| match s {
            "gain" => 1,
            "lose" => -1,
            _ => 0,
        })(input)
    }

    fn name(input: &str) -> IResult<&str, &str> {
        alpha1(input)
    }

    fn edge(input: &str) -> IResult<&str, (&str, i32, &str)> {
        tuple((
            terminated(name, tag(" would ")),
            value,
            delimited(tag(" happiness units by sitting next to "), name, tag(".")),
        ))(input)
    }

    pub fn list(input: &str) -> Result<Vec<(&str, i32, &str)>, NomErr<&str>> {
        all_consuming(terminated(
            separated_list0(line_ending, edge),
            many0(line_ending),
        ))(input)
        .finish()
        .map(|(_, v)| v)
    }
}
