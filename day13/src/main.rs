use day13::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day13.txt").unwrap();
    let input = build_edge_map(&input).unwrap();
    println!("Part 1: {}", best_path_gain(&input));
    let input = with_you(&input);
    println!("Part 2: {}", best_path_gain(&input));
}
