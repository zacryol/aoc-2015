use day25::*;

fn main() {
    let (x, y) = read_input().unwrap();
    let count = id(x, y);
    println!("Part 1: {}", codegen().nth(count as usize - 1).unwrap());
}
