use std::fs;

type Num = u64;

pub fn id(x: Num, y: Num) -> Num {
    (|a, b| (a + b) * (a + b + 1) / 2 + b)(y - 1, x - 1) + 1
}

pub fn codegen() -> impl Iterator<Item = Num> {
    std::iter::successors(Some(20151125), |i| Some((i * 252533) % 33554393))
}

pub fn read_input() -> Option<(Num, Num)> {
    fs::read_to_string("inputs/day25.txt")
        .ok()?
        .trim()
        .strip_prefix(
            "To continue, please consult the code grid in the manual.  Enter the code at row ",
        )?
        .strip_suffix('.')?
        .split_once(", column ")
        .and_then(|(y, x)| Some((x.parse().ok()?, y.parse().ok()?)))
}
