use day5::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day5.txt").unwrap();
    println!("Part 1: {}", input.lines().filter(is_nice_1).count());
    println!("Part 2: {}", input.lines().filter(is_nice_2).count());
}
