use itertools::Itertools;

fn is_vowel(c: &char) -> bool {
    matches!(c, 'a' | 'e' | 'i' | 'o' | 'u')
}

fn has_n_vowels(input: &str, n: usize) -> bool {
    input.chars().filter(is_vowel).count() >= n
}

fn no_bad_substr(input: &str) -> bool {
    const BAD_SUBSTRS: &[&str] = &["ab", "cd", "pq", "xy"];
    BAD_SUBSTRS.iter().all(|s| !input.contains(s))
}

fn has_double_char(input: &str) -> bool {
    input.chars().tuple_windows().any(|(c1, c2)| c1 == c2)
}

pub fn is_nice_1(input: &&str) -> bool {
    has_n_vowels(input, 3) && no_bad_substr(input) && has_double_char(input)
}

fn has_xyx(input: &str) -> bool {
    input.chars().tuple_windows().any(|(a, _, b)| a == b)
}

fn has_2_pair(input: &str) -> bool {
    input
        .chars()
        .tuple_windows()
        .enumerate()
        .map(|(pos, pat)| (pat, pos))
        .into_group_map::<(char, char), usize>()
        .into_iter()
        .any(|(_, v)| match &v[..] {
            [a, b] if a.abs_diff(*b) > 1 => true,
            [_, _, _, ..] => true,
            [_] => false,
            _ => false,
        })
}

pub fn is_nice_2(input: &&str) -> bool {
    has_xyx(input) && has_2_pair(input)
}
